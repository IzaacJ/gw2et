module izz0ware.common {
    export module loading {
        let callNo: number = 0;

        export function show(): void {
            $("#izz0ware-loading").addClass("izz0ware-loading--visible");
            callNo++;
        }

        export function hide(): void {
            callNo--;

            if (callNo <= 0) {
                $("#izz0ware-loading").removeClass("izz0ware-loading--visible");
                callNo = 0;
            }
        }

        export function showOnButton(btn: JQuery): void {
            btn.addClass("izz0ware-btn--loading");
            btn.addClass("izz0ware-btn--disabled");
            btn.addClass("izz0ware-btn--loading--loading");
            btn.prop("disabled", true);
        }

        export function hideOnButton(btn: JQuery): void {
            btn.removeClass("izz0ware-btn--loading");
            btn.removeClass("izz0ware-btn--disabled");
            btn.removeClass("izz0ware-btn--loading--loading");
            btn.prop("disabled", false);
        }
    }
}