module izz0ware {
    export module common {
        export function init() {
            console.log("Initializing Izz0ware JS framework...");

            console.log("- Checking Izz0ware-Loading...");
            if (!$("#izz0ware-loading").length) {
                console.log(" - Adding Loading...");
            }

            console.log("- Checking Izz0ware-Modal...");
            if (!$("#izz0ware-modal").length) {
                console.log(" - Adding Modal...");
                // add modal to DOM
            }
            if (!$("#izz0ware-modal-overlay").length) {
                console.log(" - Adding Modal overlay...");
                // add modal overlay to DOM
            }
            if (!$("#izz0ware-modal-close").length) {
                console.log(" - Adding Modal close...");
                // add modal close to DOM
            }

            console.log("- Checking Izz0ware-Message...");
            if (!$("#izz0ware-msg").length) {
                console.log(" - Adding Message...");
            }

            console.log("- Initializing resources...");
            izz0ware.common.resources.setResourceValues();
            console.log("Izz0ware JS framework initialized!");
        }

        export function scrollToElement(element: JQuery, animate: boolean = false): void {
            if (animate) {
                $("html, body").animate({
                    scrollTop: element.offset().top
                }, izz0ware.common.resources.transitionDurationMs);
            } else {
                $("html, body").scrollTop(element.offset().top);
            }
        }

        export function onEnter(element: JQuery, callback: any): void {
            element.on("keyup", function (event: JQuery.Event) {
                let code = event.keyCode || event.which;
                if (code != 13) return;
                else callback(element);
            });
        }

        export function executeFunction(fstring: string, context: any, args): void {
            args = [].slice.call(arguments).splice(2);
            let namespaces = fstring.split(".");
            let func = namespaces.pop();
            for (let i = 0; i < namespaces.length; i++) {
                context = context[namespaces[i]];
            }
            console.log(fstring);
            console.log(args);
            return context[func].apply(context, args);
        }

        export function elem(options: izz0ware.common.interfaces.IPageOptions, selector: string): JQuery {
            return options.RootElement.find("." + options.PagePrefix + selector);
        }

        export function keepSessionAlive(): void {
            setInterval(function () {
                izz0ware.common.ajax.post({
                    url: "/Shared/KeepSessionAlive",
                    dataType: "html",
                    success: function (res) {
                    }
                });
            }, 300000);
        }
    }
}