module izz0ware.common {
    export module resources {
        export let transitionDurationMs: number = 400;
        export let notificationIntervalS: number = 60;
        export let slideshowIntervalS: number = 5;
        export let facebookAppId: string;
        export let googleMapsApiKey: string;
        export let countryId: number;
        export let userId: number;
        export let isLoggedIn: boolean;

        export function setResourceValues(): void {
        }
    }
}