module izz0ware.gw2et {
    let needReorder: boolean = true;
    let autoupdate: boolean = false;

    export function init() {
        izz0ware.init();
        console.log("Izz0ware Guild Wars 2 Event Timer");
        console.log("- Initializing resources...");
        izz0ware.gw2et.resources.setResourceValues();
        $("#heartbeat").click(heartbeat);
        $("#autoupdate").click(checkAutoUpdate);

        console.log("Initialized!");
        checkAutoUpdate();
        heartbeat();
    }

    export function getEvents(elem: JQuery, formatted: boolean = true, limit: number = 0, expansion: number = -1): any {
        izz0ware.common.loading.show();
        izz0ware.common.ajax.get({
            url: izz0ware.gw2et.resources.api.getevents,
            data: {"formatted": formatted, "limit": limit, "expansion": expansion},
            dataType: "json",
            success: (result: izz0ware.common.interfaces.IJsonResult, textStatus, xhr) => {
                izz0ware.common.loading.hide();
                if (result.Ok) {
                    izz0ware.common.msg.addSuccess("Events loaded!");
                    elem.html(result.Message);
                } else {
                    izz0ware.common.msg.addError(result.Message);
                }
            },
            error: (xhr, textStatus, errorThrown) => {
                izz0ware.common.loading.hide();
                izz0ware.common.msg.addError(errorThrown);
            }
        });
    }

    export function getPreEvents(elem: JQuery, formatted: boolean = true, limit: number = 0, expansion: number = -1): any {
        izz0ware.common.loading.show();
        izz0ware.common.ajax.get({
            url: izz0ware.gw2et.resources.api.getpreevents,
            data: {"formatted": formatted, "limit": limit, "expansion": expansion},
            dataType: "json",
            success: (result: izz0ware.common.interfaces.IJsonResult, textStatus, xhr) => {
                izz0ware.common.loading.hide();
                if (result.Ok) {
                    izz0ware.common.msg.addSuccess("Events loaded!");
                    elem.html(result.Message);
                } else {
                    izz0ware.common.msg.addError(result.Message);
                }
            },
            error: (xhr, textStatus, errorThrown) => {
                izz0ware.common.loading.hide();
                izz0ware.common.msg.addError(errorThrown);
            }
        });
    }

    export function getWaypoints(elem: JQuery, formatted: boolean = true, limit: number = 0, expansion: number = -1): any {
        izz0ware.common.loading.show();
        izz0ware.common.ajax.get({
            url: izz0ware.gw2et.resources.api.getwaypoints,
            data: {"formatted": formatted, "limit": limit, "expansion": expansion},
            dataType: "json",
            success: (result: izz0ware.common.interfaces.IJsonResult, textStatus, xhr) => {
                izz0ware.common.loading.hide();
                if (result.Ok) {
                    izz0ware.common.msg.addSuccess("Events loaded!");
                    elem.html(result.Message);
                } else {
                    izz0ware.common.msg.addError(result.Message);
                }
            },
            error: (xhr, textStatus, errorThrown) => {
                izz0ware.common.loading.hide();
                izz0ware.common.msg.addError(errorThrown);
            }
        });
    }

    export function getZones(elem: JQuery, formatted: boolean = true, limit: number = 0, expansion: number = -1): any {
        izz0ware.common.loading.show();
        izz0ware.common.ajax.get({
            url: izz0ware.gw2et.resources.api.getzones,
            data: {"formatted": formatted, "limit": limit, "expansion": expansion},
            dataType: "json",
            success: (result: izz0ware.common.interfaces.IJsonResult, textStatus, xhr) => {
                izz0ware.common.loading.hide();
                if (result.Ok) {
                    izz0ware.common.msg.addSuccess("Events loaded!");
                    elem.html(result.Message);
                } else {
                    izz0ware.common.msg.addError(result.Message);
                }
            },
            error: (xhr, textStatus, errorThrown) => {
                izz0ware.common.loading.hide();
                izz0ware.common.msg.addError(errorThrown);
            }
        });
    }

    export function getExpansions(elem: JQuery, formatted: boolean = true, limit: number = 0, expansion: number = -1): any {
        izz0ware.common.loading.show();
        izz0ware.common.ajax.get({
            url: izz0ware.gw2et.resources.api.getexpansions,
            data: {"formatted": formatted, "limit": limit, "expansion": expansion},
            dataType: "json",
            success: (result: izz0ware.common.interfaces.IJsonResult, textStatus, xhr) => {
                izz0ware.common.loading.hide();
                if (result.Ok) {
                    izz0ware.common.msg.addSuccess("Events loaded!");
                    elem.html(result.Message);
                } else {
                    izz0ware.common.msg.addError(result.Message);
                }
            },
            error: (xhr, textStatus, errorThrown) => {
                izz0ware.common.loading.hide();
                izz0ware.common.msg.addError(errorThrown);
            }
        });
    }

    // Updates the timers and the UI
    export function heartbeat() {
        if(autoupdate) {
            setTimeout(heartbeat, 1000);
        }
        updateTimes('.eventlist');
        if(needReorder)
            sortTimes('.eventlist');
    }

    function updateTimes(elem: string) {
        let currenttime = new Date();
        $.each($(elem).find('.event'), function(id, e) {
            let next = null;
            let nexttxt = null;
            let duration:any = $(e).find('span.eventtimeremaining').attr('data-mduration');
            let durationms:number = duration * 60 * 1000;
            $.each($(e).find('.eventtimes .eventtime'), function (id, et) {
                let left = timeuntil($(et).html(), currenttime, false, durationms);
                if(left < next || next == null) {
                    next = left;
                    nexttxt = formattimeuntil(left - durationms);
                }
                if(left <= durationms) {
                    nexttxt = "Active for " + formattimeuntil(left);
                    $(e).toggleClass('activeevent');
                }
                if(left == 0){
                    $(e).removeClass('activeevent');
                }
            });
            $(e).find('span.eventtimeremaining').html(nexttxt);
            $(e).find('span.eventtimeremaining').attr('data-msleft', next);
            if(!needReorder && next == 0) needReorder = true;
        });
    }

    function timeuntil(time, currenttime = null, format = false, durationms = 0) {
        let offset = 1;
        let n;
        if(currenttime == null) {
            n = new Date();
        }else{
            n = currenttime;
        }
        n.setMilliseconds(0);
        let now = n.getTime() + n.getTimezoneOffset() + (offset*360000);
        let atime = time.split(":");

        let newTime = new Date();
        newTime.setHours(atime[0]);
        newTime.setMinutes(atime[1]);
        newTime.setSeconds(0);
        newTime.setMilliseconds(0);
        let newTimeStamp = newTime.getTime() + newTime.getTimezoneOffset() + (offset*360000) + durationms;

        if(newTimeStamp < now) {
            newTime.setDate(newTime.getDate()+1);
            newTimeStamp = newTime.getTime() + newTime.getTimezoneOffset() + (offset*360000) + durationms;
        }

        let left = newTimeStamp - now;
        return (format) ? formattimeuntil(left) : left;
    }

    function formattimeuntil(time: string);
    function formattimeuntil(time: number);
    function formattimeuntil(time) {
        let hours = ~~((parseInt(time)/(1000*60*60))%24);
        let minutes = ~~((parseInt(time)/(1000*60))%60);
        let seconds = ~~((parseInt(time)/1000)%60);
        let ret = "";
        if(hours > 0) {
            ret = hours + "h ";
        }
        if(minutes > 0 || hours > 0) {
            ret += minutes + "m ";
        }
        ret += seconds + "s ";
        return ret;
    }

    function sortTimes(elem: string) {
        let sortable = $(elem).isotope({
            itemSelector: '.event',
            layoutMode: 'fitRows',
            getSortData: {
                timeleft: function( itemElem ) {
                    let msleft = $(itemElem).find('span.eventtimeremaining').attr('data-msleft') + '';
                    return parseInt(msleft);
                }
            },
            sortBy: 'msleft'
        });
        sortable.isotope('updateSortData', sortable);
        sortable.isotope({
            sortBy: 'timeleft'
        });
        needReorder = false;
    }

    export function checkAutoUpdate() {
        autoupdate = $("#autoupdate").is(':checked');
        if(autoupdate) {
            $("#heartbeat").prop("disabled", true);
            heartbeat();
        }else{
            $("#heartbeat").prop("disabled", false);
        }
    }
}

$(izz0ware.gw2et.init);