module izz0ware.common {
    export module modal {
        export function show(content: string, wide: boolean = false, wider: boolean = false, centered: boolean = false, invisible: boolean = false): void {
            if (wide) $("#izz0ware-modal").addClass("izz0ware-modal--wide");
            if (wider) $("#izz0ware-modal").addClass("izz0ware-modal--wider");
            if (centered) $("#izz0ware-modal").addClass("izz0ware-modal--centered");
            if (invisible) $("#izz0ware-modal").addClass("izz0ware-modal--invisible");
            $("#izz0ware-modal").addClass("izz0ware-modal--open");
            $("#izz0ware-modal-overlay").addClass("izz0ware-modal-overlay--open");
            $("#izz0ware-modal-close").addClass("izz0ware-modal-close--open");

            $("#izz0ware-modal__scroller__content").html(content);
        }

        export function load(url: string, data: any = {}, wide: boolean = false, wider: boolean = false, centered: boolean = false, invisible: boolean = false): void {
            izz0ware.common.ajax.post({
                url: url,
                data: data,
                dataType: "html",
                success: (result: string, textStatus: string, xhr: JQueryXHR) => {
                    show(result, wide, wider, centered, invisible);
                }
            });
        }

        export function loadIframe(url: string, wide: boolean = false, wider: boolean = false, centered: boolean = false, invisible: boolean = false): void {
            show("<iframe src=\"" + url + "\" frameborder=\"0\"></iframe>", wide, wider, centered, invisible);
        }

        export function hide() {
            $("#izz0ware-modal").removeClass("izz0ware-modal--open");
            $("#izz0ware-modal-overlay").removeClass("izz0ware-modal-overlay--open");
            $("#izz0ware-modal-close").removeClass("izz0ware-modal-close--open");
            setTimeout(function () {
                $("#izz0ware-modal").removeClass("izz0ware-modal--wide");
                $("#izz0ware-modal").removeClass("izz0ware-modal--wider");
                $("#izz0ware-modal").removeClass("izz0ware-modal--centered");
                $("#izz0ware-modal").removeClass("izz0ware-modal--invisible");
            }, 500);
        }
    }
}