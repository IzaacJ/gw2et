module izz0ware.common {
    export module ajax {
        let ajaxDefaults: izz0ware.common.interfaces.IAjaxOptions = {
            url: null,
            data: {},
            async: true,
            dataType: "json",
            success: (data: any, textStatus: string, xhr: JQueryXHR) => {
            },
            error: (xhr: JQueryXHR, textStatus: string, errorThrown: string) => {
                izz0ware.common.loading.hide();
                izz0ware.common.msg.addError(textStatus);
            },
            complete: () => {
            },
            traditional: false
        };

        export function post(options: izz0ware.common.interfaces.IAjaxOptions): void {
            ajax("POST", options);
        }

        export function get(options: izz0ware.common.interfaces.IAjaxOptions): void {
            ajax("GET", options);
        }

        function ajax(type: string, options: izz0ware.common.interfaces.IAjaxOptions): void {
            options = <izz0ware.common.interfaces.IAjaxOptions> $.extend(true, {}, this.ajaxDefaults, options);

            izz0ware.common.loading.show();

            $.ajax({
                type: type,
                url: options.url,
                data: options.data,
                async: options.async,
                success: (data: any, textStatus: string, xhr: JQueryXHR) => {
                    izz0ware.common.loading.hide();

                    options.success(data, textStatus, xhr);
                },
                dataType: options.dataType,
                error: (xhr: JQueryXHR, textStatus: string, errorThrown: string) => {
                    izz0ware.common.loading.hide();
                    options.error(xhr, textStatus, errorThrown);
                },
                complete: (xhr: JQueryXHR, textStatus: string) => {
                    options.complete(xhr, textStatus);
                },
                traditional: options.traditional
            });
        }
    }
}