module izz0ware.gw2et.resources {
    export module api {
        let v = "1";
        export let getevents = "/api/v"+v+"/getevents";
        export let getpreevents = "/api/v"+v+"/getpreevents";
        export let getwaypoints = "/api/v"+v+"/getwaypoints";
        export let getzones = "/api/v"+v+"/getzones";
        export let getexpansions = "/api/v"+v+"/getexpansions";
    }

    export function setResourceValues() { }
}