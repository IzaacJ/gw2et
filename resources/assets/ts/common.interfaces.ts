module izz0ware.common {
    export module interfaces {
        export interface IJsonResult {
            Ok: boolean;
            Message: string;
        }

        export interface IAjaxOptions {
            url: string;
            data?: any;
            dataType?: string;

            success? (data: any, textStatus: string, xhr: JQueryXHR): any;

            error? (xhr: JQueryXHR, textStatus: string, errorThrown: string): any;

            complete? (xhr: JQueryXHR, textStatus: string): any;

            traditional?: boolean;
            async?: boolean;
        }

        export interface IPageOptions {
            RootElement: JQuery;
            PagePrefix: string;
            Async: boolean;
        }
    }
}