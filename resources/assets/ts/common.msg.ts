module izz0ware.common {
    export module msg {
        let transitionDuration: number = 200;
        let alertClass: string = "izz0ware-msg__msg";

        export function addSuccess(text: string) {
            this.add(text, alertClass + "--success", true);
        }

        export function addWarning(text: string) {
            this.add(text, alertClass + "--warning", false);
        }

        export function addError(text: string) {
            this.add(text, alertClass + "--error", false);
        }

        function add(text: string, className: string, autoHide: boolean = false): void {
            let notificationsJq: JQuery = $("#izz0ware-msg");
            let newAlert = $("<div class=\"" + alertClass + " " + alertClass + "--hidden " + className + "\" onclick=\"izz0ware.common.msg.remove(this)\">" + text + "</div>");
            notificationsJq.append(newAlert);
            setTimeout(function () {
                newAlert.removeClass(alertClass + "--hidden");
            }, 100);
            if (autoHide) {
                setTimeout(function () {
                    this.remove(<HTMLDivElement>newAlert[0]);
                }, 5000);
            }
        }

        export function remove(elem: HTMLDivElement) {
            let alert: JQuery = $(elem);
            alert.addClass(alertClass + "--hidden");
            setTimeout(function () {
                alert.remove();
            }, transitionDuration);
        }
    }
}