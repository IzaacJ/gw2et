<!-- resources/views/app.blade.php -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('shared.favicons')
    <meta charset="UTF-8">
    <title>Guild Wars 2 Event Timer - For streamers!</title>
    @include('shared.sources')
</head>
<body style="position: absolute; top:0;bottom:0;left:0;right:0;">
<div class="d-flex flex-column justify-content-start h-100">
    @includeWhen($header, 'partial.header')
    @includeWhen($eventlist, 'partial.eventlist', ['events' => $events])
    @include('partial.footer')
</div>
</body>
</html>