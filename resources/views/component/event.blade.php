<!-- resources/views/component/event.blade.php -->
<div class="event container">
    <div class="row">
        <div class="col eventicon"></div>
        <div class="col eventinfo">
            <div class="container-fluid">
                <div class="row">
                    <div class="col">
                        <span class="eventname">{{$e['name']}}</span>
                    </div>
                    <div class="col eventtimeremaining">
                        <span class="eventtimeremaining" data-msleft="0" data-mduration="{{$e['duration']}}">12h 32m 53s</span>
                        <span class="eventduration">{{$e['duration']}}</span>
                    </div>
                </div>
                <div class="spacer"></div>
                <div class="row">
                    <div class="col">
                        <div class="wpicon"></div>
                        <span class="eventwaypoint">{{$e['waypoint']}}</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="portalicon"></div>
                        <span class="eventzone">{{$e['zone']}}</span>
                    </div>
                    <div class="col eventcode">
                        <button class="copy"><i class="fa fa-copy"></i></button>
                        <span class="eventcode">{{$e['code']}}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="eventlocation">
        <div class="clearfix"></div>
    </div>
    <span class="eventnexttime"></span>
    <span class="eventtimes" style="display:none;">
        <pre>{{print_r($e['times'])}}</pre>
        @foreach($e['times'] as $time)
            <span class="eventtime">{{$time->time}}</span>
        @endforeach
    </span>
    <span class="preevents" style="display: none;">
        {{--
        @foreach($e['preevents'] as $pre)
            <span class="preevent" data-preid="{{$pre['id']}}">{{$pre['name']}}</span>
        @endforeach
        --}}
    </span>
</div>