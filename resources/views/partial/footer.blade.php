<!-- resources/views/partial/footer.blade.php -->
<div class="footer text-center">
    <b>GW2 Timer</b> by Izaac Johansson (https://gw2.izz0ware.info)
    @if($header)
        <br>Send me some feedback: <a href="mailto:izaacj@outlook.com?subject=GW2ET Feedback">izaacj@outlook.com</a>
    @endif
</div>