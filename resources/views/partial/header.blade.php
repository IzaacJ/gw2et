<!-- resources/views/partial/header.blade.php -->
<div class="header container-fluid align-self-start">
    <div class="row">
        <div class="col appicon"></div>
        <div class="col apptitle">
            <h2>Guild Wars 2 Event Timer</h2>
        </div>
        <div class="col appversion">v0.0.2</div>
    </div>
    <div class="row">
        <div class="col text-center">
            <p>This is an event timer that is built with streaming in mind!<br>
                What that means is that it should work perfectly fine in a BrowserSource within OBS Studio,
                and most probably in XSplit and other streaming software as well.<br>
                Just dont forget to add <code>?stream</code> or <code>?streaming</code> at the end of the url to hide this info!</p>
            @if($page !== 'about')
                <a href="/about" class="btn btn-primary">More info!</a>
                <button id="heartbeat">Heartbeat</button>
                <input type="checkbox" id="autoupdate"> Autoupdate
            @endif
        </div>
    </div>
</div>