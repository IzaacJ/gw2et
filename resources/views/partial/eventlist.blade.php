<!-- resources/views/partial/eventlist.blade.php -->
<div class="listcontainer align-self-stretch h-100">
    <div class="eventlist">
        @foreach($events as $e)
            @component('component.event', ['e' => $e])
            @endcomponent
        @endforeach
    </div>
</div>