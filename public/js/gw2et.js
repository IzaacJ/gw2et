/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
module.exports = __webpack_require__(2);


/***/ }),
/* 1 */
/***/ (function(module, exports) {

var izz0ware;
(function (izz0ware) {
    var common;
    (function (common) {
        let ajax;
        (function (ajax_1) {
            let ajaxDefaults = {
                url: null,
                data: {},
                async: true,
                dataType: "json",
                success: (data, textStatus, xhr) => {
                },
                error: (xhr, textStatus, errorThrown) => {
                    izz0ware.common.loading.hide();
                    izz0ware.common.msg.addError(textStatus);
                },
                complete: () => {
                },
                traditional: false
            };
            function post(options) {
                ajax("POST", options);
            }
            ajax_1.post = post;
            function get(options) {
                ajax("GET", options);
            }
            ajax_1.get = get;
            function ajax(type, options) {
                options = $.extend(true, {}, this.ajaxDefaults, options);
                izz0ware.common.loading.show();
                $.ajax({
                    type: type,
                    url: options.url,
                    data: options.data,
                    async: options.async,
                    success: (data, textStatus, xhr) => {
                        izz0ware.common.loading.hide();
                        options.success(data, textStatus, xhr);
                    },
                    dataType: options.dataType,
                    error: (xhr, textStatus, errorThrown) => {
                        izz0ware.common.loading.hide();
                        options.error(xhr, textStatus, errorThrown);
                    },
                    complete: (xhr, textStatus) => {
                        options.complete(xhr, textStatus);
                    },
                    traditional: options.traditional
                });
            }
        })(ajax = common.ajax || (common.ajax = {}));
    })(common = izz0ware.common || (izz0ware.common = {}));
})(izz0ware || (izz0ware = {}));
var izz0ware;
(function (izz0ware) {
    var common;
    (function (common) {
        let loading;
        (function (loading) {
            let callNo = 0;
            function show() {
                $("#izz0ware-loading").addClass("izz0ware-loading--visible");
                callNo++;
            }
            loading.show = show;
            function hide() {
                callNo--;
                if (callNo <= 0) {
                    $("#izz0ware-loading").removeClass("izz0ware-loading--visible");
                    callNo = 0;
                }
            }
            loading.hide = hide;
            function showOnButton(btn) {
                btn.addClass("izz0ware-btn--loading");
                btn.addClass("izz0ware-btn--disabled");
                btn.addClass("izz0ware-btn--loading--loading");
                btn.prop("disabled", true);
            }
            loading.showOnButton = showOnButton;
            function hideOnButton(btn) {
                btn.removeClass("izz0ware-btn--loading");
                btn.removeClass("izz0ware-btn--disabled");
                btn.removeClass("izz0ware-btn--loading--loading");
                btn.prop("disabled", false);
            }
            loading.hideOnButton = hideOnButton;
        })(loading = common.loading || (common.loading = {}));
    })(common = izz0ware.common || (izz0ware.common = {}));
})(izz0ware || (izz0ware = {}));
var izz0ware;
(function (izz0ware) {
    var common;
    (function (common) {
        let modal;
        (function (modal) {
            function show(content, wide = false, wider = false, centered = false, invisible = false) {
                if (wide)
                    $("#izz0ware-modal").addClass("izz0ware-modal--wide");
                if (wider)
                    $("#izz0ware-modal").addClass("izz0ware-modal--wider");
                if (centered)
                    $("#izz0ware-modal").addClass("izz0ware-modal--centered");
                if (invisible)
                    $("#izz0ware-modal").addClass("izz0ware-modal--invisible");
                $("#izz0ware-modal").addClass("izz0ware-modal--open");
                $("#izz0ware-modal-overlay").addClass("izz0ware-modal-overlay--open");
                $("#izz0ware-modal-close").addClass("izz0ware-modal-close--open");
                $("#izz0ware-modal__scroller__content").html(content);
            }
            modal.show = show;
            function load(url, data = {}, wide = false, wider = false, centered = false, invisible = false) {
                izz0ware.common.ajax.post({
                    url: url,
                    data: data,
                    dataType: "html",
                    success: (result, textStatus, xhr) => {
                        show(result, wide, wider, centered, invisible);
                    }
                });
            }
            modal.load = load;
            function loadIframe(url, wide = false, wider = false, centered = false, invisible = false) {
                show("<iframe src=\"" + url + "\" frameborder=\"0\"></iframe>", wide, wider, centered, invisible);
            }
            modal.loadIframe = loadIframe;
            function hide() {
                $("#izz0ware-modal").removeClass("izz0ware-modal--open");
                $("#izz0ware-modal-overlay").removeClass("izz0ware-modal-overlay--open");
                $("#izz0ware-modal-close").removeClass("izz0ware-modal-close--open");
                setTimeout(function () {
                    $("#izz0ware-modal").removeClass("izz0ware-modal--wide");
                    $("#izz0ware-modal").removeClass("izz0ware-modal--wider");
                    $("#izz0ware-modal").removeClass("izz0ware-modal--centered");
                    $("#izz0ware-modal").removeClass("izz0ware-modal--invisible");
                }, 500);
            }
            modal.hide = hide;
        })(modal = common.modal || (common.modal = {}));
    })(common = izz0ware.common || (izz0ware.common = {}));
})(izz0ware || (izz0ware = {}));
var izz0ware;
(function (izz0ware) {
    var common;
    (function (common) {
        let msg;
        (function (msg) {
            let transitionDuration = 200;
            let alertClass = "izz0ware-msg__msg";
            function addSuccess(text) {
                this.add(text, alertClass + "--success", true);
            }
            msg.addSuccess = addSuccess;
            function addWarning(text) {
                this.add(text, alertClass + "--warning", false);
            }
            msg.addWarning = addWarning;
            function addError(text) {
                this.add(text, alertClass + "--error", false);
            }
            msg.addError = addError;
            function add(text, className, autoHide = false) {
                let notificationsJq = $("#izz0ware-msg");
                let newAlert = $("<div class=\"" + alertClass + " " + alertClass + "--hidden " + className + "\" onclick=\"izz0ware.common.msg.remove(this)\">" + text + "</div>");
                notificationsJq.append(newAlert);
                setTimeout(function () {
                    newAlert.removeClass(alertClass + "--hidden");
                }, 100);
                if (autoHide) {
                    setTimeout(function () {
                        this.remove(newAlert[0]);
                    }, 5000);
                }
            }
            function remove(elem) {
                let alert = $(elem);
                alert.addClass(alertClass + "--hidden");
                setTimeout(function () {
                    alert.remove();
                }, transitionDuration);
            }
            msg.remove = remove;
        })(msg = common.msg || (common.msg = {}));
    })(common = izz0ware.common || (izz0ware.common = {}));
})(izz0ware || (izz0ware = {}));
var izz0ware;
(function (izz0ware) {
    var common;
    (function (common) {
        let resources;
        (function (resources) {
            resources.transitionDurationMs = 400;
            resources.notificationIntervalS = 60;
            resources.slideshowIntervalS = 5;
            function setResourceValues() {
            }
            resources.setResourceValues = setResourceValues;
        })(resources = common.resources || (common.resources = {}));
    })(common = izz0ware.common || (izz0ware.common = {}));
})(izz0ware || (izz0ware = {}));
var izz0ware;
(function (izz0ware) {
    let common;
    (function (common) {
        function init() {
            console.log("Initializing Izz0ware JS framework...");
            console.log("- Checking Izz0ware-Loading...");
            if (!$("#izz0ware-loading").length) {
                console.log(" - Adding Loading...");
            }
            console.log("- Checking Izz0ware-Modal...");
            if (!$("#izz0ware-modal").length) {
                console.log(" - Adding Modal...");
                // add modal to DOM
            }
            if (!$("#izz0ware-modal-overlay").length) {
                console.log(" - Adding Modal overlay...");
                // add modal overlay to DOM
            }
            if (!$("#izz0ware-modal-close").length) {
                console.log(" - Adding Modal close...");
                // add modal close to DOM
            }
            console.log("- Checking Izz0ware-Message...");
            if (!$("#izz0ware-msg").length) {
                console.log(" - Adding Message...");
            }
            console.log("- Initializing resources...");
            izz0ware.common.resources.setResourceValues();
            console.log("Izz0ware JS framework initialized!");
        }
        common.init = init;
        function scrollToElement(element, animate = false) {
            if (animate) {
                $("html, body").animate({
                    scrollTop: element.offset().top
                }, izz0ware.common.resources.transitionDurationMs);
            }
            else {
                $("html, body").scrollTop(element.offset().top);
            }
        }
        common.scrollToElement = scrollToElement;
        function onEnter(element, callback) {
            element.on("keyup", function (event) {
                let code = event.keyCode || event.which;
                if (code != 13)
                    return;
                else
                    callback(element);
            });
        }
        common.onEnter = onEnter;
        function executeFunction(fstring, context, args) {
            args = [].slice.call(arguments).splice(2);
            let namespaces = fstring.split(".");
            let func = namespaces.pop();
            for (let i = 0; i < namespaces.length; i++) {
                context = context[namespaces[i]];
            }
            console.log(fstring);
            console.log(args);
            return context[func].apply(context, args);
        }
        common.executeFunction = executeFunction;
        function elem(options, selector) {
            return options.RootElement.find("." + options.PagePrefix + selector);
        }
        common.elem = elem;
        function keepSessionAlive() {
            setInterval(function () {
                izz0ware.common.ajax.post({
                    url: "/Shared/KeepSessionAlive",
                    dataType: "html",
                    success: function (res) {
                    }
                });
            }, 300000);
        }
        common.keepSessionAlive = keepSessionAlive;
    })(common = izz0ware.common || (izz0ware.common = {}));
})(izz0ware || (izz0ware = {}));
var izz0ware;
(function (izz0ware) {
    var gw2et;
    (function (gw2et) {
        var resources;
        (function (resources) {
            let api;
            (function (api) {
                let v = "1";
                api.getevents = "/api/v" + v + "/getevents";
                api.getpreevents = "/api/v" + v + "/getpreevents";
                api.getwaypoints = "/api/v" + v + "/getwaypoints";
                api.getzones = "/api/v" + v + "/getzones";
                api.getexpansions = "/api/v" + v + "/getexpansions";
            })(api = resources.api || (resources.api = {}));
            function setResourceValues() { }
            resources.setResourceValues = setResourceValues;
        })(resources = gw2et.resources || (gw2et.resources = {}));
    })(gw2et = izz0ware.gw2et || (izz0ware.gw2et = {}));
})(izz0ware || (izz0ware = {}));
var izz0ware;
(function (izz0ware) {
    var gw2et;
    (function (gw2et) {
        let needReorder = true;
        let autoupdate = false;
        function init() {
            izz0ware.init();
            console.log("Izz0ware Guild Wars 2 Event Timer");
            console.log("- Initializing resources...");
            izz0ware.gw2et.resources.setResourceValues();
            $("#heartbeat").click(heartbeat);
            $("#autoupdate").click(checkAutoUpdate);
            console.log("Initialized!");
            checkAutoUpdate();
            heartbeat();
        }
        gw2et.init = init;
        function getEvents(elem, formatted = true, limit = 0, expansion = -1) {
            izz0ware.common.loading.show();
            izz0ware.common.ajax.get({
                url: izz0ware.gw2et.resources.api.getevents,
                data: { "formatted": formatted, "limit": limit, "expansion": expansion },
                dataType: "json",
                success: (result, textStatus, xhr) => {
                    izz0ware.common.loading.hide();
                    if (result.Ok) {
                        izz0ware.common.msg.addSuccess("Events loaded!");
                        elem.html(result.Message);
                    }
                    else {
                        izz0ware.common.msg.addError(result.Message);
                    }
                },
                error: (xhr, textStatus, errorThrown) => {
                    izz0ware.common.loading.hide();
                    izz0ware.common.msg.addError(errorThrown);
                }
            });
        }
        gw2et.getEvents = getEvents;
        function getPreEvents(elem, formatted = true, limit = 0, expansion = -1) {
            izz0ware.common.loading.show();
            izz0ware.common.ajax.get({
                url: izz0ware.gw2et.resources.api.getpreevents,
                data: { "formatted": formatted, "limit": limit, "expansion": expansion },
                dataType: "json",
                success: (result, textStatus, xhr) => {
                    izz0ware.common.loading.hide();
                    if (result.Ok) {
                        izz0ware.common.msg.addSuccess("Events loaded!");
                        elem.html(result.Message);
                    }
                    else {
                        izz0ware.common.msg.addError(result.Message);
                    }
                },
                error: (xhr, textStatus, errorThrown) => {
                    izz0ware.common.loading.hide();
                    izz0ware.common.msg.addError(errorThrown);
                }
            });
        }
        gw2et.getPreEvents = getPreEvents;
        function getWaypoints(elem, formatted = true, limit = 0, expansion = -1) {
            izz0ware.common.loading.show();
            izz0ware.common.ajax.get({
                url: izz0ware.gw2et.resources.api.getwaypoints,
                data: { "formatted": formatted, "limit": limit, "expansion": expansion },
                dataType: "json",
                success: (result, textStatus, xhr) => {
                    izz0ware.common.loading.hide();
                    if (result.Ok) {
                        izz0ware.common.msg.addSuccess("Events loaded!");
                        elem.html(result.Message);
                    }
                    else {
                        izz0ware.common.msg.addError(result.Message);
                    }
                },
                error: (xhr, textStatus, errorThrown) => {
                    izz0ware.common.loading.hide();
                    izz0ware.common.msg.addError(errorThrown);
                }
            });
        }
        gw2et.getWaypoints = getWaypoints;
        function getZones(elem, formatted = true, limit = 0, expansion = -1) {
            izz0ware.common.loading.show();
            izz0ware.common.ajax.get({
                url: izz0ware.gw2et.resources.api.getzones,
                data: { "formatted": formatted, "limit": limit, "expansion": expansion },
                dataType: "json",
                success: (result, textStatus, xhr) => {
                    izz0ware.common.loading.hide();
                    if (result.Ok) {
                        izz0ware.common.msg.addSuccess("Events loaded!");
                        elem.html(result.Message);
                    }
                    else {
                        izz0ware.common.msg.addError(result.Message);
                    }
                },
                error: (xhr, textStatus, errorThrown) => {
                    izz0ware.common.loading.hide();
                    izz0ware.common.msg.addError(errorThrown);
                }
            });
        }
        gw2et.getZones = getZones;
        function getExpansions(elem, formatted = true, limit = 0, expansion = -1) {
            izz0ware.common.loading.show();
            izz0ware.common.ajax.get({
                url: izz0ware.gw2et.resources.api.getexpansions,
                data: { "formatted": formatted, "limit": limit, "expansion": expansion },
                dataType: "json",
                success: (result, textStatus, xhr) => {
                    izz0ware.common.loading.hide();
                    if (result.Ok) {
                        izz0ware.common.msg.addSuccess("Events loaded!");
                        elem.html(result.Message);
                    }
                    else {
                        izz0ware.common.msg.addError(result.Message);
                    }
                },
                error: (xhr, textStatus, errorThrown) => {
                    izz0ware.common.loading.hide();
                    izz0ware.common.msg.addError(errorThrown);
                }
            });
        }
        gw2et.getExpansions = getExpansions;
        // Updates the timers and the UI
        function heartbeat() {
            if (autoupdate) {
                setTimeout(heartbeat, 1000);
            }
            updateTimes('.eventlist');
            if (needReorder)
                sortTimes('.eventlist');
        }
        gw2et.heartbeat = heartbeat;
        function updateTimes(elem) {
            let currenttime = new Date();
            $.each($(elem).find('.event'), function (id, e) {
                let next = null;
                let nexttxt = null;
                let duration = $(e).find('span.eventtimeremaining').attr('data-mduration');
                let durationms = duration * 60 * 1000;
                $.each($(e).find('.eventtimes .eventtime'), function (id, et) {
                    let left = timeuntil($(et).html(), currenttime, false, durationms);
                    if (left < next || next == null) {
                        next = left;
                        nexttxt = formattimeuntil(left - durationms);
                    }
                    if (left <= durationms) {
                        nexttxt = "Active for " + formattimeuntil(left);
                        $(e).toggleClass('activeevent');
                    }
                    if (left == 0) {
                        $(e).removeClass('activeevent');
                    }
                });
                $(e).find('span.eventtimeremaining').html(nexttxt);
                $(e).find('span.eventtimeremaining').attr('data-msleft', next);
                if (!needReorder && next == 0)
                    needReorder = true;
            });
        }
        function timeuntil(time, currenttime = null, format = false, durationms = 0) {
            let offset = 1;
            let n;
            if (currenttime == null) {
                n = new Date();
            }
            else {
                n = currenttime;
            }
            n.setMilliseconds(0);
            let now = n.getTime() + n.getTimezoneOffset() + (offset * 360000);
            let atime = time.split(":");
            let newTime = new Date();
            newTime.setHours(atime[0]);
            newTime.setMinutes(atime[1]);
            newTime.setSeconds(0);
            newTime.setMilliseconds(0);
            let newTimeStamp = newTime.getTime() + newTime.getTimezoneOffset() + (offset * 360000) + durationms;
            if (newTimeStamp < now) {
                newTime.setDate(newTime.getDate() + 1);
                newTimeStamp = newTime.getTime() + newTime.getTimezoneOffset() + (offset * 360000) + durationms;
            }
            let left = newTimeStamp - now;
            return (format) ? formattimeuntil(left) : left;
        }
        function formattimeuntil(time) {
            let hours = ~~((parseInt(time) / (1000 * 60 * 60)) % 24);
            let minutes = ~~((parseInt(time) / (1000 * 60)) % 60);
            let seconds = ~~((parseInt(time) / 1000) % 60);
            let ret = "";
            if (hours > 0) {
                ret = hours + "h ";
            }
            if (minutes > 0 || hours > 0) {
                ret += minutes + "m ";
            }
            ret += seconds + "s ";
            return ret;
        }
        function sortTimes(elem) {
            let sortable = $(elem).isotope({
                itemSelector: '.event',
                layoutMode: 'fitRows',
                getSortData: {
                    timeleft: function (itemElem) {
                        let msleft = $(itemElem).find('span.eventtimeremaining').attr('data-msleft') + '';
                        return parseInt(msleft);
                    }
                },
                sortBy: 'msleft'
            });
            sortable.isotope('updateSortData', sortable);
            sortable.isotope({
                sortBy: 'timeleft'
            });
            needReorder = false;
        }
        function checkAutoUpdate() {
            autoupdate = $("#autoupdate").is(':checked');
            if (autoupdate) {
                $("#heartbeat").prop("disabled", true);
                heartbeat();
            }
            else {
                $("#heartbeat").prop("disabled", false);
            }
        }
        gw2et.checkAutoUpdate = checkAutoUpdate;
    })(gw2et = izz0ware.gw2et || (izz0ware.gw2et = {}));
})(izz0ware || (izz0ware = {}));
$(izz0ware.gw2et.init);
/// <reference path="../../../node_modules/@types/jquery/index.d.ts" />
var izz0ware;
(function (izz0ware) {
    function init() {
        console.log('izz0ware init');
    }
    izz0ware.init = init;
})(izz0ware || (izz0ware = {}));


/***/ }),
/* 2 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);