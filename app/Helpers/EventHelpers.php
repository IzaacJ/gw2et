<?php
namespace App\Helpers;

class EventHelpers {
    public static function getFormattedEvents() {
        $events = \App\Event::all();
        $ret = array();
        foreach($events as $e) {
            $re = array();
            $re['name'] = $e->name;
            $wp = \App\Waypoint::find($e->waypoint_id);
            $re['waypoint'] = $wp->name;
            $re['zone'] = (\App\Zone::find($wp->zone_id))->name;
            $re['code'] = $wp->code;
            $re['times'] = $e->times()->get();
            $re['duration'] = $e->duration;
            $ret[] = $re;
        }

        return $ret;
    }
}