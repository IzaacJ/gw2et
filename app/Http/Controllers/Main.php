<?php

namespace App\Http\Controllers;

use App\Event;
use App\Eventtype;
use App\Expansion;
use App\Helpers\EventHelpers;
use App\Waypoint;
use App\Zone;
use Illuminate\Http\Request;

class Main extends Controller
{
    private $data = array(
        'query' => null,
        'method' => null,
        'events' => array(),
        'header' => true,
        'eventlist' => true,
        'page' => '',
    );
    public function __invoke(Request $request) {
        $data = array_merge($this->data, array(
            'query' => $request->query(),
            'method' => 'Main@__invoke',
            'events' => EventHelpers::getFormattedEvents(),
        ));
        return view('app', $data);
    }

    public function stream(Request $request) {
        $data = array_merge($this->data, array(
            'query' => $request->query(),
            'method' => 'Main@stream',
            'events' => EventHelpers::getFormattedEvents(),
            'header' => false,
        ));
        return view('app', $data);
    }

    public function test(Request $request) {
        $times = (\App\Event::find(1))->times()->get();
        $defined = array();
        foreach($times as $t) {
            $defined[] = $t->time;
        }
        return $defined;
    }

    public function about(Request $request) {
        $data = array_merge($this->data, array(
            'query' => $request->query(),
            'method' => 'Main@about',
            'eventlist' => false,
            'page' => 'about',
        ));
        return view('app', $data);
    }
}