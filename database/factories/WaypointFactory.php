<?php

use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Waypoint::class, function (Faker $faker) {
    $zoneids = DB::table('zones')->pluck('id')->toArray();
    $userids = DB::table('users')->pluck('id')->toArray();
    return [
        'name' => $faker->unique()->company(),
        'code' => $faker->unique()->asciify('*******'),
        'zone_id' => $faker->randomElement($zoneids),
        'game_id' => $faker->unique()->randomNumber(),
        'user_id' => $faker->randomElement($userids),
    ];
});
