<?php

use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Time::class, function (Faker $faker) {
    $userids = DB::table('users')->pluck('id')->toArray();
    return [
        'time' => $faker->unique()->time(),
        'user_id' => $faker->randomElement($userids),
    ];
});
