<?php

use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Eventtype::class, function (Faker $faker) {
    $userids = DB::table('users')->pluck('id')->toArray();
    return [
        'name' => $faker->unique()->jobTitle(),
        'user_id' => $faker->randomElement($userids),
    ];
});
