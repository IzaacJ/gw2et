<?php

use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Expansion::class, function (Faker $faker) {
    $userids = DB::table('users')->pluck('id')->toArray();
    return [
        'name' => $faker->unique()->company(),
        'released' => $faker->date(),
        'user_id' => $faker->randomElement($userids),
    ];
});
