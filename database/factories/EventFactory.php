<?php

use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Event::class, function (Faker $faker) {
    $wpids = DB::table('waypoints')->pluck('id')->toArray();
    $eventtypeids = DB::table('eventtypes')->pluck('id')->toArray();
    $userids = DB::table('users')->pluck('id')->toArray();
    $name = $faker->unique()->name();
    return [
        'name' => $name,
        'duration' => $faker->numberBetween(15, 30),
        'waypoint_id' => $faker->randomElement($wpids),
        'eventtype_id' => $faker->randomElement($eventtypeids),
        'image_path' => 'https://dummyimage.com/150/000/fff.png&text='.urlencode($name),
        'user_id' => $faker->randomElement($userids),
    ];
});
