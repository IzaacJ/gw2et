<?php

use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Eventtime::class, function (Faker $faker) {
    $eventids = DB::table('events')->pluck('id')->toArray();
    $timeids = DB::table('times')->pluck('id')->toArray();
    $userids = DB::table('users')->pluck('id')->toArray();
    return [
        'event_id' => $faker->randomElement($eventids),
        'time_id' => $faker->unique()->randomElement($timeids),
        'user_id' => $faker->randomElement($userids),
    ];
});
