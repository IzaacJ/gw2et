<?php

use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Zone::class, function (Faker $faker) {
    $expids = DB::table('expansions')->pluck('id')->toArray();
    $userids = DB::table('users')->pluck('id')->toArray();
    return [
        'name' => $faker->unique()->city(),
        'expansion_id' => $faker->randomElement($expids),
        'user_id' => $faker->randomElement($userids),
    ];
});
