<?php

use Illuminate\Database\Seeder;
use Symfony\Component\Console\Output\ConsoleOutput;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $output = new ConsoleOutput();
        $output->writeln('Seeding users...');
        factory(App\User::class, 50)->create();
        $output->writeln('Seeding expansions...');
        factory(App\Expansion::class, 3)->create();
        $output->writeln('Seeding zones...');
        factory(App\Zone::class, 50)->create();
        $output->writeln('Seeding waypoints...');
        factory(App\Waypoint::class, 150)->create();
        $output->writeln('Seeding times...');
        factory(App\Time::class, 300)->create();
        $output->writeln('Seeding eventtypes...');
        factory(App\Eventtype::class, 10)->create();
        $output->writeln('Seeding events...');
        factory(App\Event::class, 50)->create();
        $output->writeln('Seeding eventtimes...');
        factory(App\Eventtime::class, 300)->create();
        $output->writeln('Seeding Done!');
    }
}
