<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaypointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waypoints', function (Blueprint $table) {
            $table->increments('id');          // Waypoint ID
            $table->integer('game_id');        // Waypoint ID in game
            $table->string('name');            // Waypoint Name
            $table->char('code', 7);    // Waypoint code
            $table->smallInteger('zone_id');   // Zone ID
            $table->timestamps();                      // Waypoint added
            $table->integer('user_id');        // Added by User with ID
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('waypoints');
    }
}
