<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventtimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventtimes', function (Blueprint $table) {
            $table->increments('id');       // Event->Time ID
            $table->integer('event_id');    // Event ID
            $table->integer('time_id');     // Time ID
            $table->timestamps();                   // Event->Time added
            $table->integer('user_id');     // Added by User with ID
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventtimes');
    }
}
