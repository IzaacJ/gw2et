<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');               // Event ID
            $table->string('name');                 // Event name
            $table->smallInteger('duration');       // Event duration
            $table->smallInteger('waypoint_id');    // Waypoint ID
            $table->smallInteger('eventtype_id');   // Eventtype ID
            $table->string('image_path');           // Event Image Path
            $table->timestamps();                           // Event added
            $table->integer('user_id');             // Added by User with ID
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
